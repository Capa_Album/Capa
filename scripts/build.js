/**
 * Created by manicqin on 27/10/16.
 */

'use strict';

const electron = require('electron');
const app = electron.app;

const spawn = require( 'child_process' ).spawn;
// const bob = spawn( 'node-gyp',
//         [ 'build' , `--target=${process.versions.electron}`, '--dist-url=https://atom.io/download/atom-shell'],
//         {cwd:`${__dirname}/node_modules/leveldown`} );

const bob = spawn( 'node-gyp',
    [ 'build' , `--target=${process.versions.electron}`, '--dist-url=https://atom.io/download/atom-shell'],
    {cwd:`${__dirname}`} );

bob.stdout.on( 'data', data => {
    console.log( `${data}` );
});

bob.stderr.on( 'data', data => {
    console.log( `stderr: ${data}` );
});

bob.on( 'close', code => {
    console.log( `child process exited with code ${code}` );
    app.quit();
});