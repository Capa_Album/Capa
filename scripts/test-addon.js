/**
 * Created by manicqin on 30/11/16.
 */

const electron = require('electron')
const app = electron.app
const _ = require('lodash')
const thumber  = require('../src/server/create_thumbs')
const path_module   = require('path')
const config        = require('../src/server/config_manager').fetch_configuration()

app.on('ready', function () {

    const path = "/home/manicqin/Projects/Capa/images/images_2"
    const thumb_folder = path_module.join(app.getPath("pictures"), config.gallery.thumbnail_directory)


    thumber.create_thumbs_from_path(path, thumb_folder).then( result => {
        console.log("bulkDocs Ok", result)

        app.quit()
    }).catch(err => {
        console.log("this is err2", err)
        app.quit()
    })
})