/**
 * Created by manicqin on 28/05/17.
 */

'use strict';
const electron = require('electron');
const app = electron.app;
const _ = require('lodash')

const old_config = require('../src/server/config_manager').fetch_configuration()

function getObjectDiff(obj1, obj2) {
    const diff = Object.keys(obj1).reduce((result, key) => {
        if (!obj2.hasOwnProperty(key)) {
            result.push(key);
        } else if (_.isEqual(obj1[key], obj2[key])) {
            const resultKeyIndex = result.indexOf(key);
            result.splice(resultKeyIndex, 1);
        }
        return result;
    }, Object.keys(obj2));

    return diff;
}

const config = {
    windowState:{
        isMaximized:false,
        isFullScreen:false,
        bounds:{
            x:0,
            y:0,
            width:800,
            height:600
        }
    },
    ui:{
        width:250,
        height:250,
        row_pagination:10,
        column_Max:4,
        item_width:200,
        item_height:200
    },
    gallery:{
        thumbnail_directory:"thumbnails"
    },
    gallery_db:{
        name:"gallery",
        path:"db/"
    },
    notification_db:{
        name:"notification",
        path:"db/"
    },
    request_db:{
        name:"request",
        path:"db/"
    },
    maps_db:{
        name:"maps",
        path:"db/"
    }
}

if(old_config !== null)
{
    console.log(getObjectDiff(old_config,config))
}
console.log(old_config)
require('../src/server/config_manager').store_configuration(config, process.env.HOME)
app.quit()