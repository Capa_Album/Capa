const Ajv = require('ajv')
const Protocol = require('../src/shared/protocol')

let setupAsync = require('ajv-async');
let ajv = setupAsync(new Ajv);

const EXIF = {
    ImageDescription: "",
    Make : "Apple",
    Model : "iPhone 6 Plus",
    Software : "8.1.2",
    DateTime : "2015:09:28 16:25:22",
    DateTimeOriginal : "2015:09:28 16:25:22",
    DateTimeDigitized : "2015:09:28 16:25:22",
    SubSecTimeOriginal : "684",
    Copyright : "",
    ByteAlign : 0,
    Orientation : 1,
    BitsPerSample : 0,
    ExposureTime : 0.030303030303030304,
    FNumber : 2.2,
    ISOSpeedRatings : 32,
    ShutterSpeedValue : 0,
    ExposureBiasValue : 0,
    SubjectDistance : 0,
    FocalLength : 4.15,
    FocalLengthIn35mm : 29,
    Flash : 0,
    MeteringMode : 5,
    ImageWidth : 3264,
    ImageHeight : 2448,
    GeoLocation: {
        Latitude: 0,
        Longitude: 0,
        Altitude: 0,
        AltitudeRef: 0,
        DOP: 0,
        LatComponents: {
            degrees : 0,
            minutes : 0,
            seconds : 0,
            direction : 63
        },
        LonComponents : {
            degrees : 0,
            minutes : 0,
            seconds: 0,
            direction: 63
        }
    },
    LensInfo: {
        FocalLengthMax : 4.15,
        FocalLengthMin : 4.15,
        FStopMax : 2.2,
        FStopMin : 2.2,
        FocalPlaneYResolution : 0,
        FocalPlaneXResolution: 0,
        Make : "Apple",
        Model : "iPhone 6 Plus back camera 4.15mm f/2.2"
    }
}
ajv.addSchema([Protocol.LensInfoSchema, Protocol.GeoLocationSchema, Protocol.LocationComponentsSchema])
valid = ajv.validate(Protocol.EXIFSchema, EXIF)
    .then(valid => {console.log(valid)})
    .catch(err => { console.log('Validation errors:', err.errors) })

