/**
 * Created by manicqin on 15/05/17.
 */

const _     = require('lodash')

var express = require('express')
const body_parser    = require('body-parser')

const srvr = express()
srvr.use(body_parser.json())

const config = require('../src/server/config_manager').fetch_configuration()

let db_management = require('../src/server/db_management')
let db = new db_management.DbManagement()
db.init_db()
//db.listen_changes(config.gallery_db.name, event => console.log("event received",event))

function GetAllDocs(req, res){
    return db.query(config.gallery_db.name)
}

function QueryDocs(arg){
    return db.query(config.gallery_db.name, arg)
}

function getDoc(arg){
    return db.get_doc(config.gallery_db.name, arg)
}

function getAttach(arg){
    return db.get_attachment(config.gallery_db.name, arg)
}

srvr.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
})

srvr.get('/readDB', (req, res) => {
    let ret = undefined
    console.log('query', req.query)

    if (_.isEmpty(req.query)){
        ret = GetAllDocs()
    }
    else{
        ret = QueryDocs(req.query)
    }

    ret.then(docs => { res.send(docs) })
        .catch(err => console.log(err))
})

function print_doc(doc){
    console.log(doc.doc._id, doc.doc._attachments)
}

GetAllDocs()
    .then(docs => docs.rows.forEach(print_doc))
    .catch(err => console.trace(err))

srvr.get('/readAttachment', (req, res) => {
    let ret = "Needs params"


    console.log('query', req.query)
    if (!_.isEmpty(req.query)){

        getAttach(req.query)
            .then(docs => {
                console.log(docs.length, docs)
                res.type('png')
                res.send(docs)
            })
            .catch(err => console.trace(err))
    }
    else {
        res.send(ret)
    }

})


let port = 3000
srvr.listen(port, function () {
    console.log(`Example app listening on port ${port}!`)
})

