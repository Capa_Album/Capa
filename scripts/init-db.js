const electron = require('electron')
const app = electron.app

//const config = require('../src/server/config_manager').fetch_configuration()
let db_management = require('../src/server/db_management')


app.on('ready', function () {

    console.log('ready')
    db_management.drop_and_recreate_db().then(function (result) {
        console.log('Successfuly destroyed galleryController', result)
        app.quit()
    }).catch(err => {
        console.log(err)
        app.quit()
    })
})

// (function () {

// const date_index = {
//     index: {
//         fields: ['capa_data.date_original']
//     },
//     "name": "date_original",
//     "type": "json"
//
// }
//
// const device_index = {
//     index: {
//         fields: ['capa_data.device']
//     },
//     "name": "device",
//     "type": "json"
//
// }
//
// const tag_index = {
//     index: {
//         fields: ['capa_data.tag']
//     },
//     "name": "tag",
//     "type": "json"
//
// }

// function init_db() {
//     console.log('ready')
//     return db.destroy_gallery().then(result => {
//
//         console.log('Successfuly destroyed galleryController', result)
//         db.init_db()
//
//         return Promise.all([
//             db.create_index(config.gallery_db.name, date_index),
//             db.create_index(config.gallery_db.name, device_index),
//             db.create_index(config.gallery_db.name, tag_index),
//         ]).then(result => {
//             console.log("Successfuly Created indexes")
//             return db.get_indexes(config.gallery_db.name)
//         })
//     })
// }
//
//
// exports.init_db = init_db
// }).call(this)
//


// var PouchDB = require('pouchdb')
// PouchDB.plugin(require('pouchdb-find'))
//
// PouchDB.debug.enable('*')
// PouchDB.debug.enable('pouchdb:api')
//
// let prefixed = PouchDB.defaults({
//     prefix: config.gallery_db.path
// });
//
// var galleryController = new prefixed(config.gallery_db.name, {db : require('leveldown')})
//var mapsController = new PouchDB('mapsController', {db : require('leveldown')})