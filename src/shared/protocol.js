module.exports.Protocol = {

    READ_ALBUM:'READ_ALBUM',
    READ_ALBUM_OK:'READ_ALBUM_OK',
    READ_ALBUM_FAIL:'READ_ALBUM_FAIL',
    READ_ALBUM_PENDING:'READ_ALBUM_PENDING',

    PING:'PING',
    PING_OK:'PONG',
    PING_FAIL:'',
    PING_PENDING:'',

    CLEAR_ALBUM:'CLEAR_ALBUM',
    CLEAR_ALBUM_OK:'CLEAR_ALBUM_OK',
    CLEAR_ALBUM_FAIL:'CLEAR_ALBUM_FAIL',
    CLEAR_ALBUM_PENDING:'CLEAR_ALBUM_PENDING',

    LOAD_SOURCES:'LOAD_SOURCES',
    LOAD_SOURCES_OK:'LOAD_SOURCES_OK',
    LOAD_SOURCES_FAIL:'LOAD_SOURCES_FAIL',
    LOAD_SOURCES_PENDING:'LOAD_SOURCES_PENDING',

    FILTER:'FILTER',
    FILTER_OK:'FILTER_OK',
    FILTER_FAIL:'FILTER_FAIL',
    FILTER_PENDING:'FILTER_PENDING',

    NOP:'NOP'
}

module.exports.Routes = {
    REQUESTS:'/requests',
    NOTIFICATIONS:'/notifications'
}

module.exports.Codes = {
    Ok:'OK',
    Error:'ERROR'
}

module.exports.QueryTypes = {
    Tag:'Tag',
    Device:'Device',
    Date:'Date'
}

module.exports.LensInfoSchema = {
    "$id": "http://capa.org/schemas/LensInfoSchema.json",
    "type": "object",
    "required":["Make","Model"],
    "properties": {
        "FocalLengthMax": { "type": "number" },
        "FocalLengthMin": { "type": "number" },
        "FStopMax": { "type": "number" },
        "FStopMin": { "type": "number" },
        "FocalPlaneYResolution": { "type": "number" },
        "FocalPlaneXResolution": { "type": "number" },
        "Make": { "type": "string" },
        "Model": { "type": "string" }
    }
}

module.exports.GeoLocationSchema = {
    "$id": "http://capa.org/schemas/GeoLocationSchema.json",
    "type": "object",
    "required":["LatComponents","LonComponents"],
    "properties": {
        "Latitude": { "type": "number" },
        "Longitude": { "type": "number" },
        "Altitude": { "type": "number" },
        "AltitudeRef": { "type": "number" },
        "DOP": { "type": "number" },
        "LatComponents": { "$ref": "LocationComponentsSchema.json"},
        "LonComponents": { "$ref": "LocationComponentsSchema.json"}
    }
}

module.exports.LocationComponentsSchema = {
    "$id": "http://capa.org/schemas/LocationComponentsSchema.json",
    "type": "object",
    "properties": {
        "degrees": { "type": "number" },
        "minutes": { "type": "number" },
        "seconds": { "type": "number" },
        "direction": { "type": "number" }
    }
}

module.exports.EXIFSchema = {
    "$async": true,
    "$id": "http://capa.org/schemas/EXIFSchema.json",
    "type": "object",
    "required":["ImageWidth","ImageHeight"],
    "properties": {
        "ImageDescription": { "type": "string" },
        "Make": { "type": "string" },
        "Model": { "type": "string" },
        "Software": { "type": "string" },
        "DateTime": { "type": "string", "patternRequired": [ "\\d+:\\d+:\\d+ \\d+:\\d+:\\d+" ]  },
        "DateTimeOriginal": { "type": "string", "patternRequired": [ "\\d+:\\d+:\\d+ \\d+:\\d+:\\d+" ]  },
        "DateTimeDigitized": { "type": "string", "patternRequired": [ "\\d+:\\d+:\\d+ \\d+:\\d+:\\d+" ]  },
        "SubSecTimeOriginal": { "type": "string" },
        "Copyright": { "type": "string" },
        "ByteAlign": { "type": "number" },
        "Orientation": { "type": "number" },
        "BitsPerSample": { "type": "number" },
        "ExposureTime": { "type": "number" },
        "FNumber": { "type": "number" },
        "ISOSpeedRatings": { "type": "number" },
        "ShutterSpeedValue": { "type": "number" },
        "ExposureBiasValue": { "type": "number" },
        "SubjectDistance": { "type": "number" },
        "FocalLength": { "type": "number" },
        "FocalLengthIn35mm": { "type": "number" },
        "Flash": { "type": "number" },
        "MeteringMode": { "type": "number" },
        "ImageWidth": { "type": "number" },
        "ImageHeight": { "type": "number" },
        "GeoLocation":{ "$ref": "GeoLocationSchema.json"},
        "LensInfo":{"$ref": "LensInfoSchema.json"}
    }
}

module.exports.EXIFLightSchema = {
    "$async": true,
    "$id": "http://capa.org/schemas/EXIFSchema.json",
    "type": "object",
    "required":["ImageWidth","ImageHeight"],
    "properties": {
        "ImageDescription": { "type": "string" },
        "Make": { "type": "string" },
        "Model": { "type": "string" },
        "Software": { "type": "string" },
        "DateTime": { "type": "string", "patternRequired": [ "\\d+:\\d+:\\d+ \\d+:\\d+:\\d+" ]  },
        "DateTimeOriginal": { "type": "string", "patternRequired": [ "\\d+:\\d+:\\d+ \\d+:\\d+:\\d+" ]  },
        "DateTimeDigitized": { "type": "string", "patternRequired": [ "\\d+:\\d+:\\d+ \\d+:\\d+:\\d+" ]  },
        "SubSecTimeOriginal": { "type": "string" },
        "Copyright": { "type": "string" },
        "Orientation": { "type": "number" },
        "ImageWidth": { "type": "number" },
        "ImageHeight": { "type": "number" },
        "GeoLocation":{ "$ref": "GeoLocationSchema.json"},
    }
}
