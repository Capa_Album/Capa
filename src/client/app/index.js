/**
 * Created by manicqin on 27/02/17.
 */
import {createStore,applyMiddleware} from 'redux'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import App from './components/App'
import galleryApp from './reducers/'
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant'
import thunkMiddleware from 'redux-thunk'

const store = createStore(galleryApp, applyMiddleware(thunkMiddleware,reduxImmutableStateInvariant()))

render(
    <div>
        <Provider store={store}>
            <App/>
        </Provider>
    </div>,
    document.getElementById('root')
)