/**
 * Created by manicqin on 27/12/16.
 */
import _ from 'lodash'
import axios from 'axios'
import {Protocol,Routes} from '../../../shared/protocol'
const ipc = require('electron').ipcRenderer

const url = 'http://localhost:3000'

function create_action(type,items){
    return {
        type:type,
        items
    }
}

export function convertNotificationstoActions(server_action){

    let lst = server_action.data.rows.map(obj => {
        return create_action(obj.doc.type, obj.doc.items)
    })

    if(_.isEmpty(lst))
        return [create_action(Protocol.NOP)]
    else
        return lst

}

export function readAlbum(args) {
    return function (dispatch){
        dispatch(create_action(Protocol.READ_ALBUM_PENDING))
        axios.post(url+Routes.REQUESTS, create_action(Protocol.READ_ALBUM, args))
    }
}


export function ping() {
    return function (dispatch){
        dispatch(create_action(Protocol.PING_PENDING))
        axios.post(url+Routes.REQUESTS, create_action(Protocol.PING))
    }
}

export  function clearAlbum() {
    return function (dispatch){
        dispatch(create_action(Protocol.CLEAR_ALBUM_PENDING))
        axios.post(url+Routes.REQUESTS, create_action(Protocol.CLEAR_ALBUM))
    }
}


export  function loadSource() {
    return function (dispatch){
        dispatch(create_action(Protocol.LOAD_SOURCES_PENDING))
        ipc.send('open-file-dialog')
    }
}


function readNotifications_func(query){
    return axios.get(url+Routes.NOTIFICATIONS)
}

export function readNotifications(){
    return dispatch => {
        readNotifications_func()
            .then(data => {
                const act = convertNotificationstoActions(data)
                act.forEach(item => {dispatch(item)})
            })
            .catch(err => console.error(err))

    }
}

// export function actLoadImageListData(imgListData){
//     return create_action('LOAD_IMAGE_LIST_DATA', imgListData)
// }

// export function actLoadImage(imgPath){
//     const action = {
//         type: 'LOAD_IMAGE',
//         imgPath
//     }
//     return action
// }

// function imagesList_func(query){
//     return axios.get('http://localhost:3000/test1', {
//         params: query
//     })
// }

// export function loadAllImages(){
//     return dispatch => {
//         imagesList_func().then((data) => {
//
//             let images = _.remove(_.map(data.data.rows,'doc'), item => item.EXIF !== undefined)
//             dispatch(actLoadImageListData(images))
//         }).catch(err => console.error(err))
//
//     }
// }
//
// export function queryImages(query){
//     return dispatch => {
//
//         imagesList_func(query).then((data) => {
//             dispatch(actLoadImageListData(data.data.docs))
//         }).catch(err => console.error(err))
//
//     }
// }