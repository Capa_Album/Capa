/**
 * Created by manicqin on 27/12/16.
 */

import { combineReducers } from 'redux'
import {Protocol} from '../../../shared/protocol'

function executeGalleryAction(state= [], action) {

    switch (action.type){

        case Protocol.PING_OK:
            return state

        case Protocol.LOAD_SOURCES_OK:
            return state

        case Protocol.CLEAR_ALBUM_OK:
            return []

        case Protocol.READ_ALBUM_OK:
            return action.items.rows.map(item => item.doc)

        case Protocol.READ_ALBUM_PENDING:
        case Protocol.LOAD_SOURCES_PENDING:
            console.info(action)

        case Protocol.NOP:
            return state

        default:
            console.debug(action)

    }

    return state
}

const galleryApp = combineReducers({
    images:executeGalleryAction
})


export default galleryApp