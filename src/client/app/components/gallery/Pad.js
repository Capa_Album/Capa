/**
 * Created by manicqin on 07/12/16.
 */
import React, { PropTypes } from 'react'
import _ from 'lodash'
import { Image } from 'react-bootstrap'

let padStyles = {
    border: '0px',
    borderRadius: 5,
    cursor: 'pointer',
    padding: '0px',
    margin: '5px 0px 0px 5px ',
    width:'250px',
    height:'250px',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundColor:'red'
}

// const Pad = ({onClick, imgPath}) => (
//     <div
//         onClick={onClick}
//         style={padStyles}
//     />
// )
//
// Pad.propTypes = {
//     onClick: PropTypes.func.isRequired,
//     imgPath: PropTypes.string.isRequired
// }

//////////////////////////////////////////////////////////////////////////////////////////////

    class Pad extends React.Component{

        constructor(props) {
            super(props)
        }

        componentWillMount(){

            let style = _.clone(padStyles)
            style["backgroundImage"] = this.props.imgPath
            this.setState({style:style})
        }

        render() {
            const thumbnail = this.state.style.backgroundImage
            const data = thumbnail.data.data

            const blob = new Blob([new Uint8Array(data)], {type: thumbnail.content_type})
            const url = URL.createObjectURL(blob)

            return (
                <img src={url}/>
             )
        }
}

Pad.propTypes = {
    imgPath: PropTypes.object.isRequired
}
export default Pad