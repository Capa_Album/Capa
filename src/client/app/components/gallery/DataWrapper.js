/**
 * Created by manicqin on 12/12/16.
 */
import React, { PropTypes }  from 'react';
import Pad from './Pad';
import Button from '../button/Button';
import { Label } from 'react-bootstrap'
const padStyles = {
    border: '0px',
    borderRadius: 5,
    backgroundColor: 'black',
    cursor: 'pointer',
    padding: '0px 0px 0px 0px',
    margin: 2,
    width:'260px',
    height:'260px',
    float:'left',

}

const  DataWrapper = React.createClass({
    handleDetails: function (e){

        e.preventDefault()
        console.log(this.props.imgData)
    },
    render: function() {

        let {imgData, handleOpen} = this.props;
        console.log(imgData.capa_data)
        return  (<div style={padStyles}>
            <Pad imgPath={imgData._attachments["thumbnail.png"]}/>
            <h3><Label> {imgData.EXIF.DateTimeOriginal} </Label></h3>
            <p>
                <Button onClick={handleOpen} title="Open" />
                <Button onClick={this.handleDetails} title="Details" />
            </p>
        </div>)
    }
})

Pad.DataWrapper = {
    imgData: PropTypes.object.isRequired,
    handleOpen: PropTypes.func.isRequired
}
export default DataWrapper