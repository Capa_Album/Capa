/**
 * Created by manicqin on 11/12/16.
 */

import React from 'react'
import DataWrapper from './DataWrapper'
import Gallery_Button from '../button/Button'
import {connect} from 'react-redux'
import {clearAlbum, readNotifications, ping, readAlbum, loadSource} from '../../actions'
import Timer from '../timer/Timer'
import DatePicker from "react-bootstrap-date-picker"

const galleryStyles = {
    border: '0px',
    borderRadius: 0,
    backgroundColor: '#ffffcc',
    cursor: 'pointer',
    padding: 0,
    margin: 0,
    width:'1200px',
    minHeight: '500px'
}


class Gallery extends React.Component{

    constructor(props, context){

        super(props, context)
        this.handle_OnClickDetails = this.handle_OnClickDetails.bind(this)
        this.handle_OnClickAddSource = this.handle_OnClickAddSource.bind(this)
        this.handle_OnDispatch = this.handle_OnDispatch.bind(this)
        this.handle_OnPing = this.handle_OnPing.bind(this)
        this.handle_OnReadAlbum = this.handle_OnReadAlbum.bind(this)
        this.handle_OnReadAlbumDate = this.handle_OnReadAlbumDate.bind(this)
        this.handle_OnReadAlbumTag = this.handle_OnReadAlbumTag.bind(this)

        this.handle_DatePick = this.handle_DatePick(this)
        this.handle_Tick = this.handle_Tick.bind(this)

        this.begin_date = new Date().toISOString()
        this.end_date = new Date().toISOString()


    }

    handle_DatePick(value, formattedValue){
        this.begin_date = formattedValue
    }

    handle_OnPing(event){
        event.preventDefault()
        this.props.dispatch(ping())
    }

    handle_OnReadAlbum(event){
        event.preventDefault()
        this.props.dispatch(readAlbum())
    }

    handle_OnReadAlbumDate(event){
        event.preventDefault()

        // Access ISO String and formatted values from the DOM.
        let hiddenInputElement = document.getElementById("datepicker-begin");
        const Start = hiddenInputElement.getAttribute('data-formattedvalue') // Formatted String, ex: "11/19/2016"


        // Access ISO String and formatted values from the DOM.
        hiddenInputElement = document.getElementById("datepicker-end");
        const End = hiddenInputElement.getAttribute('data-formattedvalue')

        const args = {
            Type:"Date",
            Start,
            End
        }

        this.props.dispatch(readAlbum(args))
    }

    handle_OnReadAlbumTag(event){
        event.preventDefault()
        this.props.dispatch(readAlbum({Type:"Tag",Tag:"Maker"}))
    }

    handle_OnDispatch(event){
        event.preventDefault()
        this.props.dispatch(clearAlbum())

    }

    handle_OnClickAddSource(event){
        event.preventDefault()
        this.props.dispatch(loadSource())
    }

    handle_OnClickDetails(e){

        e.preventDefault()

    }

    handle_Tick(e){

        this.props.dispatch(readNotifications())

    }

    render() {

        let {images} = this.props

        let children = undefined
        if (images === undefined) children = (<div> None </div>)
        else
        {
            children = images.map((image,index) => {
                return <DataWrapper key={index} imgData={image} handleOpen={this.handle_OnClickDetails}/>
            })
        }
        return (<div>
                    <center>
                        <Gallery_Button title="Ping" onClick={this.handle_OnPing}/>
                        <Gallery_Button title="Add source" onClick={this.handle_OnClickAddSource}/>
                        <Gallery_Button title="Clear" onClick={this.handle_OnDispatch}/>
                        <div>
                            <Gallery_Button title="Read Album" onClick={this.handle_OnReadAlbum}/>
                        </div>
                        <div>
                            <DatePicker id="datepicker-begin" value={this.begin_date} onChange={this.handle_DatePick} />
                            <DatePicker id="datepicker-end" value={this.end_date} onChange={this.handle_DatePick} />
                            <Gallery_Button title="Read Album Date" onClick={this.handle_OnReadAlbumDate}/>
                        </div>
                        <div>
                            <Gallery_Button title="Read Album Tag" onClick={this.handle_OnReadAlbumTag}/>
                        </div>
                    </center>
                    <div style={galleryStyles}>
                        {children}
                    </div>
                    <Timer tick={this.handle_Tick} interval={500} />
                </div> )
    }
}

function mapStateToProps(state, ownProps){
    return {
        images:state.images
    }
}

export default connect(mapStateToProps)(Gallery)