/**
 * Created by manicqin on 06/12/16.
 */
import React from 'react'
import { Button } from 'react-bootstrap'

// const buttonStyles = {
//     border: '1px solid #eee',
//     borderRadius: 3,
//     backgroundColor: '#FFFFFF',
//     cursor: 'pointer',
//     fontSize: 15,
//     padding: 0,
//     margin: 5,
// }

const Gallery_Button = ({ title, onClick }) => (
<Button bsStyle="primary" onClick={onClick}>
    {title}
</Button>
)

Gallery_Button.propTypes = {
    title: React.PropTypes.string.isRequired,
    onClick: React.PropTypes.func,
}

export default Gallery_Button