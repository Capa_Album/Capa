/**
 * Created by manicqin on 05/07/17.
 */
import React, { PropTypes } from 'react'

class Timer extends React.Component{

    constructor(props) {
        super(props)

        this.tick       = props.tick
        this.interval   = props.interval
    }

    componentWillMount(){

        this.timer = setInterval(this.tick, this.interval)
    }

    componentWillUnmount(){

         clearInterval(this.timer)
    }

    render() {
        return false
    }
}

Timer.propTypes = {
    tick: PropTypes.func.isRequired,
    interval:PropTypes.number.isRequired
}
export default Timer