'use strict';

const cluster = require('cluster')
const path_module = require('path')
const os = require('os')

const config_manager = require('./server/config_manager')
const thumber = require('./server/create_thumbs')
const db = require('./server/db_management')
const {Protocol} = require('./shared/protocol')

require('dotenv').config()

//TODO: extract to class
function handle_request(new_event) {
    console.log("event received", new_event.doc)

    let request_map = {}
    request_map[Protocol.PING] = new_event => {
        return db.push_notification({type: Protocol.PING_OK}) }

    request_map[Protocol.CLEAR_ALBUM] = new_event => {
            return db.drop_and_recreate_db()
                .then(result => {
                    return db.push_notification({type: Protocol.CLEAR_ALBUM_OK})
                })
                .catch(err => {
                    return db.push_notification({type: Protocol.CLEAR_ALBUM_FAILED, description: err})
                })
        }

    request_map[Protocol.LOAD_SOURCES] = new_event => {
            //TODO: Get rid of the thumb_folder
            const thumb_folder = path_module.join(os.homedir(), 'Pictures', config_manager.fetch_configuration().gallery.thumbnail_directory)

            return Promise.all(new_event.doc.items.map(item => {
                //TODO: change name to from source
                return thumber.create_thumbs_from_path(item, thumb_folder)

            }))
                .then(result => {
                    return db.push_notification({type: Protocol.LOAD_SOURCES_OK, items: result.length})
                })
                .catch(err => {
                    return db.push_notification({type: Protocol.LOAD_SOURCES_FAILED, description: err})
                })
        }

    request_map[Protocol.READ_ALBUM] = new_event => {

        return db.query(require('./server/config_manager.js').fetch_configuration().gallery_db.name, new_event.doc.items)
            .then(result => {
                return db.push_notification({type: Protocol.READ_ALBUM_OK, items: result})
            })
            .catch(err => {
                console.trace(err)
                return db.push_notification({type: Protocol.READ_ALBUM_FAILED, description: err})
            })
    }


    if(request_map.hasOwnProperty(new_event.doc.type))
    {
        let test = data => {
            console.log("sadfghjk")
            console.info(data)
        }
        request_map[new_event.doc.type](new_event)
            .then(test)
            .catch(console.trace)
    }
    else
    {
        console.error("Unknown action type: ", new_event.doc.type)
    }
}

let workers = []
if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`)
    require('./server/server.js')

    if(process.env.HEADLESS == 0)
    {
        require('./server/electron_app.js')
    }

    let db_management = require('./server/db_management.js')
    const config = require('./server/config_manager.js').fetch_configuration()
    let db = new db_management.DbManagement()
    db.init_db()
    db.listen_changes(config.request_db.name, handle_request)
    // const numCPUs = require('os').cpus().length
    //
    // // Fork workers.
    // for (let i = 0; i < numCPUs; i++) {
    //     workers.push(cluster.fork())
    //     workers[i].send({type:"Exit"})
    // }
    //
    // cluster.on('exit', (worker, code, signal) => {
    //     console.log(`worker ${worker.process.pid} died ${code} ${signal}`)
    // })
} else {

    console.log(`Worker ${process.pid} started`)
    process.on('message', (msg) => {
        if (msg.type === "Exit") {
            process.exit(0)
        }
    })
}
