/**
 * Created by manicqin on 09/05/17.
 */
const body_parser   = require('body-parser')

const config        = require('./config_manager.js').fetch_configuration()
const db_management   = require('./db_management.js')

const {Routes, Codes} = require('../shared/protocol')
const express = require('express')

let db = new db_management.DbManagement()
db.init_db()

let srvr = express()
srvr.use(body_parser.json())

function GetAllDocs(req, res){
    return db.query(config.gallery_db.name)
}

function QueryDocs(arg){
    return db.query(config.gallery_db.name,arg)
}

if(process.env.HEADLESS == 1)
{
    srvr.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    })
}

srvr.post(Routes.REQUESTS, (req, res) => {
    console.info(Routes.REQUESTS, req.body, Date.now())
    db.add_document(config.request_db.name,req.body)
        .then(result =>  res.send(Codes.OK))
        .catch(err => {
            res.send(Codes.Error,err)
            console.log(Routes.REQUESTS, err)
        })
})

srvr.get(Routes.NOTIFICATIONS, (req, res) => {

    db_management.pop_notification()
        .then(result => {
            if(result.total_rows) {
                console.info(Routes.NOTIFICATIONS, Date.now())
            }
            res.send(result)
        })
        .catch(err => {
            console.trace(Routes.NOTIFICATIONS,err)
            res.send(Codes.Error)
        })

})

let port = process.env.PORT
srvr.listen(port, () => {
    console.info(`Listening on port ${port}!`)
})