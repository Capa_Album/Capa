/**
 * Created by manicqin on 29/04/17.
 */

(function() {
    //require('electron').app.getPath('userData')
    exports.fetch_configuration = function (location) {
        location = (location !== undefined)?location:process.env.HOME
        let JSONStorage = require('node-localstorage').JSONStorage
        let local_storage = new JSONStorage(location)
        return local_storage.getItem('config_object')
    }
    exports.store_configuration = function (config_object, location) {
        location = (location !== undefined)?location:process.env.HOME
        let JSONStorage = require('node-localstorage').JSONStorage
        let local_storage = new JSONStorage(location)
        local_storage.setItem('config_object', config_object)
    }
}).call(this)