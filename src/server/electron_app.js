/**
 * Created by manicqin on 09/05/17.
 */

const electron = require('electron')
const app = electron.app
const BrowserWindow = electron.BrowserWindow
let mainWindow

const fs = require('fs')
const ipc = require('electron').ipcMain
const dialog = require('electron').dialog

let windowState = {}

const config_manager    = require('./config_manager.js')
const axios             = require('axios')
const {Routes, Protocol} = require('../shared/protocol')

const url = 'http://localhost:3000'
function storeWindowState() {
    windowState.isMaximized = mainWindow.isMaximized()
    if (!windowState.isMaximized) {
        // only update bounds if the window isn't currently maximized
        windowState.bounds = mainWindow.getBounds()
    }

    let config = config_manager.fetch_configuration()
    config.windowState = windowState
    config_manager.store_configuration(config)
    //global.nodeStorage.setItem('windowstate', windowState)
}

function createWindow () {

    try {
        windowState = config_manager.fetch_configuration().windowState
    } catch (err) {
        console.log(err)
    }

    console.log(windowState)
    if(!windowState){
        windowState = {
            isMaximized:false
        }
    }

    mainWindow = new BrowserWindow({
        title:  'Capa',
        x:      windowState && windowState.bounds && windowState.bounds.x || undefined,
        y:      windowState && windowState.bounds && windowState.bounds.y || undefined,
        width:  windowState && windowState.bounds && windowState.bounds.width || 800,
        height: windowState && windowState.bounds && windowState.bounds.height || 600,
    })

    // Restore maximised state if it is set.
    // not possible via options so we do it here
    if (windowState && windowState.isMaximized) {
        mainWindow.maximize()
    }

    //mainWindow.setFullScreen(true);
    // and load the index.html of the app.
    mainWindow.loadURL(`file://${__dirname}/../client/index.html`)

    // Open the DevTools.
    //mainWindow.webContents.openDevTools();
    mainWindow.onbeforeunload = (e) => {

        storeWindowState()
        // Unlike usual browsers that a message box will be prompted to users, returning
        //   // a non-void value will silently cancel the close.
        //     // It is recommended to use the dialog API to let the user confirm closing the
        //       // application.
        e.returnValue = false
    }
    mainWindow.on('move', () => {
        storeWindowState()
    })

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.

        mainWindow = null
    })

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }

})

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }

})

ipc.on('open-file-dialog', event => {
    dialog.showOpenDialog({
        properties: ['openDirectory']
    }, items => {

        if (items){
            const action = {
                type:Protocol.LOAD_SOURCES,
                items
            }

            axios.post(url+Routes.REQUESTS, action).then(data => console.log("pushed requests")).catch(err => console.trace(err))

        }
    })
})