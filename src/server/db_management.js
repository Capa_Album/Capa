/**
 * Created by manicqin on 10/05/17.
 */
const _             = require('lodash')

let PouchDB = require('pouchdb')
const config_manager = require('./config_manager.js')
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('pouchdb-adapter-memory'));

let instance = null
const config = config_manager.fetch_configuration()
module.exports.DbManagement = class {
    constructor() {
        if (instance)
            return instance

        instance = this
        this.dbs = {}
        this.listeners = {}
        this.is_init = false
    }

    init_db() {
        if (this.is_init == false) {

            let prefixed = PouchDB.defaults({
                prefix: config.gallery_db.path
            });

            this.dbs[config.gallery_db.name] = new prefixed(config.gallery_db.name)
            this.dbs[config.request_db.name] = new PouchDB(config.request_db.name, {adapter: 'memory'})
            this.dbs[config.notification_db.name] = new PouchDB(config.notification_db.name, {adapter: 'memory'})
            this.dbs.maps = '{"Map":{"Tag":"000","Min":{"X":0,"Y":0},"Max":{"X":100,"Y":100},"Segments":[{"Map":{"Tag":"001","Min":{"X":0,"Y":0},"Max":{"X":50,"Y":50},"Segments":[{"Map":{"Tag":"011","Min":{"X":0,"Y":0},"Max":{"X":25,"Y":25},"Segments":[{"Map":{"Tag":"311","Min":{"X":0,"Y":0},"Max":{"X":50,"Y":50}}},{"Map":{"Tag":"312","Min":{"X":25,"Y":25},"Max":{"X":50,"Y":50}}},{"Map":{"Tag":"313","Min":{"X":20,"Y":20},"Max":{"X":40,"Y":40}}}]}},{"Map":{"Tag":"012","Min":{"X":25,"Y":0},"Max":{"X":50,"Y":25}}},{"Map":{"Tag":"013","Min":{"X":0,"Y":25},"Max":{"X":25,"Y":50}}},{"Map":{"Tag":"014","Min":{"X":25,"Y":25},"Max":{"X":50,"Y":50}}}]}},{"Map":{"Tag":"002","Min":{"X":50,"Y":0},"Max":{"X":100,"Y":50}}},{"Map":{"Tag":"003","Min":{"X":0,"Y":50},"Max":{"X":50,"Y":100}}},{"Map":{"Tag":"004","Min":{"X":50,"Y":50},"Max":{"X":100,"Y":100}}}]}}'
            this.is_init = true
        }
    }

    destroy_gallery() {
        this.is_init = false
        return this.dbs[config.gallery_db.name].destroy()
    }

    create_index(db_name, index) {
        return this.dbs[db_name].createIndex(index)
    }

    get_indexes(db_name) {
        return this.dbs[db_name].getIndexes()
    }

    query(db_name, arg) {
        let retval = null
        switch (db_name) {
            case config.maps_db.name:
                retval = Promise.resolve(this.dbs[db_name])
                break

            case config.gallery_db.name:
            case config.request_db.name:
            case config.notification_db.name:

                if (arg == undefined) {

                    retval = this.dbs[db_name]
                        .allDocs({include_docs: true, attachments: true, binary: true, endkey: '_design'})

                } else {

                    retval = this.dbs[db_name].query((doc, emit) => {
                        if(doc.capa_data.date_original !== null &&
                            doc.capa_data.date_original !== undefined) {
                            emit(doc.capa_data.date_original.toString())
                        }
                        else{
                            emit('undefined')
                        }

                    },{
                        startkey     : arg.date_start.toString(),
                        endkey       : arg.date_end.toString()+'\ufff0',
                        include_docs : true,
                        attachments: true,
                        binary:true
                    } )
                }
                break;

            default:
                retval = Promise.reject('None Shall Pass')
        }

        return retval
    }

    // query(db_name, arg) {
    //     let retval = null
    //     switch (db_name) {
    //         case config.maps_db.name:
    //             retval = Promise.resolve(this.dbs[db_name])
    //             break
    //
    //         case config.gallery_db.name:
    //         case config.request_db.name:
    //         case config.notification_db.name:
    //
    //             if (arg !== undefined) {
    //
    //                 retval = this.dbs[db_name]
    //                     .find(arg)
    //             }
    //             else {
    //                 retval = this.dbs[db_name]
    //                     .allDocs({include_docs: true, attachments: true, binary: true, endkey: '_design'})
    //             }
    //             break;
    //
    //         default:
    //             retval = Promise.reject('None Shall Pass')
    //     }
    //
    //     return retval
    // }

    listen_changes(db_name, handle_change) {
        this.listeners[db_name] = this.dbs[db_name]
            .changes({live: true, include_docs: true, since: 'now'})
            .on('change', handle_change)
            .on('error', console.trace)
    }

    unlisten_changes(db_name) {
        this.listeners[db_name].cancel()
    }

    add_document_array(db_name, docs) {
        return this.dbs[db_name].bulkDocs(docs)
    }

    add_document(db_name, docs) {
        return this.dbs[db_name].post(docs)
    }

    get_doc(db_name, doc_id) {
        return this.dbs[db_name]
            .get(doc_id, {attachments: true, binary: true})
    }

    get_attachment(db_name, doc) {
        return this.dbs[db_name]
            .getAttachment(doc.id, doc.thumb)
    }

    put_doc(db_name, doc) {
        console.info("Pushing to DB:", db_name, " Data: ", doc)
        return this.dbs[db_name].put(doc)
    }

    remove_doc(db_name, removable_doc) {
        let delete_doc = removable_doc
        delete_doc["_deleted"] = true

        return this.dbs[db_name].put(delete_doc)
    }
}

const date_index = {
    index: {
        fields: ['capa_data.date_original']
    },
    "name": "date_original",
    "type": "json"

}

const device_index = {
    index: {
        fields: ['capa_data.device']
    },
    "name": "device",
    "type": "json"

}

const tag_index = {
    index: {
        fields: ['capa_data.tag']
    },
    "name": "tag",
    "type": "json"

}

module.exports.query = (db, args) => {
    let db_manager = new module.exports.DbManagement()
    let query = undefined

    if(args != undefined && args.hasOwnProperty("Type")) {
        console.log(args)
        switch (args.Type) {
            case 'Date': {
                const date_start = new Date(args.Start)
                const date_end = new Date(args.End)

                query = {
                    date_start : date_start.getTime(),
                    date_end : date_end.getTime()
                }

//                return db_manager.query(db, find_date)

            }
            case 'Tag': {
                const find_device = {
                    selector: {'capa_data.device': {$regex: 'iPhone'}},
                }

//                return db_manager.query(db, find_device)

            }
        }
    }

    return db_manager.query(db, query)
}

module.exports.push_notification = function (notification) {
    let db = new module.exports.DbManagement()
    db.init_db()
    notification["_id"] = new Date().getTime().toString()
    return db.put_doc(config.notification_db.name, notification)
}

module.exports.pop_notification = function () {

    let db = new module.exports.DbManagement()
    db.init_db()
    return db.query(config.notification_db.name)
        .then(docs => {
            Promise.all(docs.rows.map(doc => {
                return db.remove_doc(config.notification_db.name, doc.doc)
            }))
                .catch(console.warn)

            return Promise.resolve(docs)
        })
        .catch(err => {
            return Promise.reject(err)
        })
}

module.exports.drop_and_recreate_db = function () {

    let db = new module.exports.DbManagement()
    db.init_db()
    return db.destroy_gallery()
        .then(result => {

            //TODO: there's a small bug here currently you'l not notice him the function is supposed to init only the gallery controller but inits all dbs.
            console.log('Destroying galleryController:', result)
            db.init_db()

            return Promise.all([
                db.create_index(config.gallery_db.name, date_index),
                db.create_index(config.gallery_db.name, device_index),
                db.create_index(config.gallery_db.name, tag_index),
                ])
                .then(result => {
                    console.log("Creating indexes:",result)
                    return db.get_indexes(config.gallery_db.name)
                })
        })
}

