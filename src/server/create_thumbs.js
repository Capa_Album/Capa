/**
 * Created by manicqin on 25/06/17.
 */

(function () {
    const DBMng = require('./db_management').DbManagement
    const utils = require('./utils')
    const cil = require("capa_image_lib")
    const config = require("./config_manager").fetch_configuration()
    const fs = require('fs')
    const _ = require('lodash')

    function scan_source(path) {
        let file_list = []

        utils.walkSync(path, file => file_list.push({_id: file}))

        if (file_list.length == 0)
            return Promise.reject('No files found')

        let db = new DBMng
        db.init_db()

        return db.add_document_array(config.gallery_db.name, file_list)
    }

    function read_exif(file_list) {
        return cil.get_exif(file_list)
    }


    function create_thumbs_from_path(path, thumb_folder) {

        return scan_source(path)
            .then(function (result) {

                let docs = []
                result.forEach(obj => {
                    if (obj.ok == true
                        || (obj.docs != undefined
                            && obj.docs[0].error != undefined
                            && obj.docs[0].error.error)) {

                        let exif_object = cil.get_one_exif(obj.id)
                        if (exif_object.EXIF === undefined) {
                            console.warn(`Cannot extract EXIF for file ${obj.id}`)
                            const picture_info = cil.get_picture_info([obj.id])

                            exif_object = picture_info[0]
                        }

                        console.log(JSON.stringify(exif_object))
                        exif_object['Filename'] = obj.id
                        exif_object['_rev'] = obj.rev
                        docs.push(exif_object)

                    }
                })

                if (docs.length == 0) {
                    console.warn("we've found 0 files")
                    return Promise.resolve([])
                }
                docs.forEach(obj => {

                    const thumb_data = utils.prepare_thumb_data(obj)

                    const thumb_buffer = cil.create_thumb_buffer(thumb_data.original_filename,
                        thumb_data.orientation,
                        thumb_data.width,
                        thumb_data.height)

                    obj._id = thumb_data.original_filename
                    obj.capa_data = {}
                    obj["_attachments"] = {
                        "thumbnail.png": {
                            content_type: 'image/png',
                            data: thumb_buffer
                        }
                    }

                    utils.create_indices(obj)
                })
                let db = new DBMng
                db.init_db()

                return db.add_document_array(config.gallery_db.name, docs)
                   // .then(docs => db.add_document(config.notification_db.name, {type:"NEW_SOURCES_LOADED",count:docs.length}))

            }).catch(err => Promise.reject(err))
    }


    exports.create_thumbs_from_path = create_thumbs_from_path
}).call(this)