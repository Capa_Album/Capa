/**
 * Created by manicqin on 29/05/17.
 */


(function () {
    let walkSync;
    const fs = require('fs')
    const _ = require('lodash')
    const path_module = require('path')
    const config = require('./config_manager').fetch_configuration()

    function add_presuffix_to_filename(filename_full_path, suffix) {
        const tmp_ext = path_module.extname(filename_full_path)
        const tmp_base = path_module.basename(filename_full_path, tmp_ext)
        //const dir_name = path_module.dirname(filename_full_path)

        return path_module.format({

            //dir: dir_name,
            name: tmp_base + suffix,
            ext: tmp_ext
        })
    }

    //TODO: add file filter
    walkSync = function (currentDirPath, callback) {
        let func = (currentDirPath, callback) => {
            fs.readdirSync(currentDirPath).forEach(function (name) {
                const filePath = path_module.join(currentDirPath, name)
                const stat = fs.statSync(filePath)
                if (stat.isFile()) {
                    callback(filePath, stat)
                } else if (stat.isDirectory()) {
                    walkSync(filePath, callback)
                }
            })
        }

        if (_.isArray(currentDirPath)) {
            currentDirPath.forEach(file => {
                func(file, callback)
            })
        }
        else {
            func(currentDirPath, callback)
        }
    }


    function create_index_date(exif_doc) {
        //TODO: what to do when we don't have DateTimeOriginal?
        if (exif_doc.DateTimeOriginal != undefined) {
            const key = exif_doc.DateTimeOriginal.split(' ')[0]
            const split_date = key.split(':')
            const year = split_date[0]
            const month = split_date[1]
            const day = split_date[2]

            return parseInt(Date.parse(`${month} ${day} ${year}`).toString())
        }
        return null
    }

    function create_index_device(exif_doc) {
        const model = exif_doc.Model;
        return model
    }

    function create_indices(exif_doc) {
        let success = false

        if (exif_doc !== undefined && exif_doc.EXIF !== undefined) {
            exif_doc.capa_data.date_original = create_index_date(exif_doc.EXIF)
            exif_doc.capa_data.device = create_index_device(exif_doc.EXIF)
            exif_doc.capa_data.tags = []
        }

        return success
    }

    function is_landscape(exif_doc) {
        if (parseInt(exif_doc.ImageWidth) > parseInt(exif_doc.ImageHeight)) {
            return true
        }
        return false
    }

    function prepare_thumb_data(exif_doc, presuffix, thumb_path) {
        const filename = exif_doc.Filename
        // const filename_resize = add_presuffix_to_filename(filename, presuffix)
        // const thumb_path = process.cwd()+require('path').sep+config.gallery.thumbnail_directory

        let w = 0;
        let ratio = 0;
        let h = 0;
        if (is_landscape(exif_doc.EXIF)) {

            const thumb_max_width = config.ui.item_width
            w = thumb_max_width
            ratio = exif_doc.EXIF.ImageWidth / w
            h = exif_doc.EXIF.ImageHeight / ratio   //todo: what happens if picture is smaller than thumb
        } else {

            const thumb_max_height = config.ui.item_height
            h = thumb_max_height
            ratio = exif_doc.EXIF.ImageHeight / h
            w = exif_doc.EXIF.ImageWidth / ratio   //todo: what happens if picture is smaller than thumb
        }

        const filenames = {
            original_filename: filename,
            // thumb_filename : require('path').join(thumb_path, filename_resize),
            orientation: exif_doc.EXIF.Orientation,
            width: w,
            height: h
        }
        return filenames
    }

    exports.add_presuffix_to_filename = add_presuffix_to_filename
    exports.prepare_thumb_data = prepare_thumb_data
    exports.is_landscape = is_landscape
    exports.create_indices = create_indices
    exports.walkSync = walkSync
}).call(this)